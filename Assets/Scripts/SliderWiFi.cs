﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderWifi : MonoBehaviour
{
	public GameObject _panelMenu;

	public void ShowWifi()
	{
		if (_panelMenu != null)
		{
			Animator animator = _panelMenu.GetComponent<Animator>();
			if (animator != null)
			{
				bool isOpened = animator.GetBool("IsWiFiOpened");
				animator.SetBool("IsWiFiOpened", !isOpened);
			}
		}
	}
}